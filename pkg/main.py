import time
from typing import Union

from pydantic import BaseModel, validator
from svgpathtools import svg2paths, Line, Arc, CubicBezier, QuadraticBezier

from config import *


class GcodeGenerator:
    def __init__(self, svg_file_path: str, scale: float = 1, rotation: complex = 0, translation: complex = 0):
        self.generated_gcode: str = ""
        self.svg_paths = None
        self.head_state = "UP"  # TODO put both states in dataclass or sth DOWN | UP
        self.scale = scale
        self.rotation = rotation  # in degrees
        self.translation = translation  # in degrees
        self.svg_file_path = svg_file_path
        self.current_pos = 0 + 0j

    def is_close_to_head(self, position: complex):
        dist = ((self.current_pos.real + position.real) ** 2 + (self.current_pos.imag + position.imag) ** 2) ** 0.5
        return dist < CLOSE_POINT_MM

    def set_translation(self, new_translation):
        self.translation = new_translation

    def generate(self) -> str:
        """
        Generate gcode
        :return: generated gcode
        """
        self.append_starting_gcode()
        self.read_svg()
        self.generate_gcode()
        self.append_end_gcode()
        return self.generated_gcode

    def save_gcode(self, output_path) -> None:
        """
        Saves generated gcode to an output_path
        :param output_path: doesn't generate folder structure
        """
        with open(output_path, "w") as f:
            f.write(self.generated_gcode)

    def set_scale(self, new_scale: float) -> None:
        """
        Set the scale of the svg, doesn't affect translation.
        :param new_scale:
        """
        self.scale = new_scale

    def set_rotation(self, new_rotation: float) -> None:
        """
        Set the rotation of the svg, doesn't affect translation.
        :param new_scale:
        """
        self.rotation = new_rotation

    def get_generated_gcode(self) -> str:
        """
        Get the gcode that was generated
        """
        return self.generated_gcode

    def append_gcode_command(self, command: str, comment: str = "") -> bool:
        """
        Adds gcode command to gcode file
        :param comment: comment for the command
        :param command: command to be added
        :return: True if operation succeeds
        """
        # adds gcode command and comment if exists
        self.generated_gcode += f"{command}{f'  ;{comment}' if comment else ''}\n"
        # TODO some validation
        return True

    def lift_head(self) -> None:
        """
        Lifts the head of the machine e.g. don't draw during future movements
        """
        if self.head_state != "DOWN":
            return
        self.append_gcode_command(f"M3 S0", "lift head")
        self.head_state = "UP"

    def lower_head(self):
        """
        Lowers the head of the machine e.g. draw during future movements
        """
        if self.head_state != "UP":
            return
        self.append_gcode_command(f"M3 S255", "lift head")
        self.head_state = "DOWN"

    def append_starting_gcode(self, gcode: str = None) -> None:
        """
        Append starting gcode to the generated gcode.
        :param gcode: if present adds as starting gcode, else takes it from file
        """
        if gcode:
            self.generated_gcode += gcode
            return

        with open(STARTING_GCODE_PATH, "r") as f:
            starting_gcode = f.readlines()
        for command in starting_gcode:
            self.append_gcode_command(command)

    def append_end_gcode(self, gcode: str = None) -> None:
        """
        Append end gcode to the generated gcode
        :param gcode: if present adds as end gcode, else takes it from file
        """
        if gcode:
            self.generated_gcode += gcode
            return

        with open(END_GCODE_PATH, "r") as f:
            end_gcode = f.readlines()
        for command in end_gcode:
            self.append_gcode_command(command)

    def read_svg(self) -> None:
        """
        Reads svg from the path specified before.
        """
        # TODO exceptions
        self.svg_paths, attributes = svg2paths(self.svg_file_path)
        print(self.svg_paths)
        print("------")
        # for k, v in enumerate(attributes):
        # print(k, v)
        # print(f"read svg paths:")
        # for path in self.svg_paths:
        #     print(path)
        #     print()

    def process_curve(self, line: Union[Arc, CubicBezier, QuadraticBezier]) -> None:
        # TODO even distribution of points
        if not self.is_close_to_head(line.start):
            self.lift_head()
        self.move_to(line.start, idle=True)
        self.lower_head()
        line_len = line.length(0, 1)
        segments = int((line_len / 10) * CURVES_SEGMENTS_PER_10_MM) + 1
        for i in range(0, segments + 1):
            self.move_to(line.point(i / segments), idle=False)

    def move_to(self, coords: complex, idle: bool = True) -> None:
        """
        Moves head of the machine to specified coordinates
        :param coords: where to move to
        :param idle: idle movements use idle movement (faster)
        """
        x_cord = round(coords.real + self.translation.real, FLOATING_POINT_PRECISION)
        y_cord = round(coords.imag + self.translation.imag, FLOATING_POINT_PRECISION)
        self.append_gcode_command(
            f"{'G0' if idle else 'G1'} "
            f"X{x_cord} "
            f"Y{y_cord} "
            f"F{FEEDRATE_XY_IDLE if idle else FEEDRATE_XY_PRINT}")

        self.current_pos = complex(x_cord, y_cord)

    def count_all_paths(self) -> int:
        """
        Get number of all paths in read svg file.
        :return: Number of all paths in read svg file.
        """
        count = 0
        for path in self.svg_paths:
            count += len(path)
        return count

    def generate_gcode(self) -> None:
        """
        Main function to generate gcode, with the parameters given before.
        """
        number_of_paths = self.count_all_paths()
        progress = 0
        for path in self.svg_paths:
            for line in path:
                line = line.scaled(self.scale, self.scale, 0j)
                line = line.rotated(self.rotation, 0j)
                if isinstance(line, Line):
                    if not self.is_close_to_head(line.start):
                        self.lift_head()
                    self.move_to(line.start, idle=True)
                    self.lower_head()
                    self.move_to(line.end, idle=False)
                else:
                    self.process_curve(line)

                progress += 1
                print(f"{round(progress / number_of_paths * 100, 2)}%")


if __name__ == "__main__":
    start_time = time.time()

    generator = GcodeGenerator(svg_file_path='pkg/fixtures/beer.svg', scale=0.5, translation=1 + 1j)
    generator.generate()
    generator.save_gcode(OUTPUT_PATH)

    print(f'Generation time: {(time.time() - start_time) * 1000}ms')
