# machine dimensions
MACHINE_MAX_X = 300
MACHINE_MAX_Y = 400
MACHINE_MAX_Z = 50
MACHINE_MIN_X = 0
MACHINE_MIN_Y = 0
MACHINE_MIN_Z = 0

# files paths
STARTING_GCODE_PATH = "pkg/gcodes/starting.gcode"
END_GCODE_PATH = "pkg/gcodes/end.gcode"

OUTPUT_PATH = "pkg/output.gcode"

# speeds and feeds
HEAD_UP_HEIGHT = 10.0
HEAD_DOWN_HEIGHT = 0
FEEDRATE_XY_IDLE = 700.0
FEEDRATE_XY_PRINT = 700.0
FEEDRATE_Z = 10.0

# precision
FLOATING_POINT_PRECISION = 4  # rounds all coordinates to < decimal places
CURVES_SEGMENTS_PER_10_MM = 2
CLOSE_POINT_MM = 2 # mm