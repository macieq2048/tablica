import Vue from 'vue'
import Router from 'vue-router'
import {ActiveUser} from '@/services/user';


// Containers
const TheContainer = () => import('@/containers/TheContainer');

// Views - Pages
const Page404 = () => import('@/views/pages/Page404');
const Login = () => import('@/views/pages/Login');
const ForgotPassword = () => import('@/views/pages/ForgotPassword');
const ResetPassword = () => import('@/views/pages/ResetPassword');
const Register = () => import('@/views/pages/Register');
const HelpPage = () => import('@/views/pages/HelpPage');

// Admin
const EngagementSyncLog = () => import('@/views/admin/EngagementSyncLog');
const UserActivity = () => import('@/views/admin/UserActivity')

// InputFiles
const InputFiles = () => import('@/views/input_files/InputFiles');
const FileData = () => import('@/views/input_files/FileData');

// Analytics
const Analytics = () => import('@/views/analytics/Analytics');

// Engagements
const Engagements = () => import('@/views/engagements/Engagements');

// User
const User = () => import('@/views/users/UserView');
const Users = () => import('@/views/users/UsersView');

Vue.use(Router);


const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  scrollBehavior: () => ({y: 0}),
  routes: configRoutes()
});

const isOpenAccess = (route) =>
  route.matched.some((route) => route.meta?.isOpenAccess);

const isFound = (route) => route.matched[0].name !== "NotFound";

router.beforeEach((to, from, next) => {
  if (to.meta?.getTitle) to.meta.title = to.meta.getTitle(to)
  const isAuthenticated = ActiveUser.get();

  if (!isAuthenticated && !isOpenAccess(to)) {
    if (isFound(to)) {
      localStorage.setItem('pathToLoadAfterLogin', to.path)
    }
    return next({name: 'Login'});
  }
  return next();
});
export default router


function configRoutes() {
  return [
    {path: '/', redirect: '/engagements'},
    {
      path: '/engagements',
      meta: {label: 'Engagements'},
      component: TheContainer,
      children: [
        {
          path: '',
          name: 'Engagements',
          component: Engagements,
        },
        {
          path: ':engagementName',
          meta: {
            label: 'Engagement',
            paramKey: 'engagementName',
          },
          component: {
            render(c) {
              return c('router-view');
            },
          },
          children: [
            {
              path: '',
              name: 'Engagement',
              redirect: 'input-files',
            },
            {
              path: 'input-files',
              name: 'Input Files',
              component: {
                render(c) {
                  return c('router-view');
                },
              },
              children: [
                {
                  path: '',
                  name: "Input Files",
                  component: InputFiles,
                },
                {
                  path: ':fileType',
                  meta: {
                    paramKey: 'fileType',
                    getTitle: to =>
                      `${to.params.fileType[0].toUpperCase() + to.params.fileType.slice(1).replace('_', ' ')} - ${to.query.tab || 'Merge'}`
                  },
                  name: 'File Name',
                  component: FileData
                },
              ]
            },
            {
              path: 'analytics',
              name: 'Analytics View',
              redirect: 'analytics/journal-entry',
              component: {
                render(c) {
                  return c('router-view');
                },
              },
              children: [
                {
                  path: ':fileType',
                  meta: {paramKey: 'fileType'},
                  name: 'File name',
                  component: Analytics,
                },
              ]
            },
            {
              path: 'audit-results',
              name: 'Audit Results',
              component: Analytics,
            },
          ],
        }
      ]
    },
    {
      path: '/account',
      name: 'Account',
      meta: {
        isOpenAccess: true,
      },
      component: {
        render(c) {
          return c('router-view')
        }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login,
        },
        {
          path: 'login_management',
          name: 'ManagementLogin',
          component: Login,
          props: {
            show_email_login: true
          }
        },
        {
          path: 'forgot',
          name: 'Forgot password',
          component: ForgotPassword,
        },
        {
          path: 'reset/:token',
          name: 'Password reset',
          component: ResetPassword,
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        }
      ]
    },
    {
      path: '/oauth',
      name: 'OAuth Login',
      meta: {
        isOpenAccess: true,
      },
      component: Login
    },
    {
      path: '/help',
      name: '',
      component: TheContainer,
      children: [
        {
          path: '',
          name: '',
          component: HelpPage
        },
      ]
    },
    {
      path: '/admin',
      name: '',
      component: TheContainer,
      children: [
        {
          path: 'engagement-sync-log',
          name: 'Engagement Sync Log',
          component: EngagementSyncLog
        },
        {
          path: 'users',
          name: 'Users',
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: '',
              name: 'Users',
              component: Users
            },
            {
              path: ':id',
              name: 'User',
              meta: {paramKey: 'id'},
              component: User
            }
          ]
        },
        {
          path: 'user-activity',
          name: 'User Activity',
          component: UserActivity,
        },
      ]
    },
    {
      path: '/users',
      name: 'Users',
      component: {
        render(c) {
          return c('router-view')
        }
      },
      children: [
        {
          path: 'me',
          name: 'Me',
          component: TheContainer,
          children: [
            {
              path: '',
              name: 'Me',
              component: User
            },
          ],
        },
      ]
    },
    {
      path: "*",
      name: 'NotFound',
      component: TheContainer,
      children: [
        { path: '*', component: Page404 }
      ]
    }
  ]
}
