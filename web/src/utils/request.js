import {encode} from 'querystring';

export const request = async ({
                                url,
                                query,
                                headers = {},
                                method = 'GET',
                                body,
                                formData,
                                ...rest
                              }) => {
  const methodLower = method.toLowerCase();
  if (
    body &&
    (methodLower === 'post' ||
      methodLower === 'put' ||
      methodLower === 'patch' ||
      methodLower === 'delete') &&
    !headers['Content-Type']
  ) {
    headers['Content-Type'] = 'application/json';
  }
  if (body && typeof body === 'object') {
    body = JSON.stringify(body);
  }
  if (formData) {
    body = formData;
  }
  const res = await fetch(url + (query ? `?${encode(query)}` : ''), {
    method,
    headers,
    body,
    ...rest,
  });

  let data;
  try {
    data = await res.json();
  } catch {
    data = {message: res.statusText};
  }

  return {
    res,
    data,
    headers: res.headers,
  };
};