import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: () => import(/* webpackChunkName: "about" */ '../views/MainPage')
  },
    {
    path: '/gcode',
    name: 'gcode',
    component: () => import(/* webpackChunkName: "about" */ '../components/gcode_viewer/GcodeViewer')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
