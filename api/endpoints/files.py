from fastapi import FastAPI, File, UploadFile, APIRouter

router = APIRouter(
    prefix="/api/upload-file",
    tags=["upload file"],
)

@router.post("/")
async def create_upload_file(file: UploadFile = File(...)):
    contents = await file.read()
    with open(f"./uploaded_files/{file.filename}", "wb") as f:
        f.write(contents)
    return {"filename": file.filename}