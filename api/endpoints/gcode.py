from fastapi import APIRouter
from svg_to_gcode.compiler import Compiler, interfaces
from svg_to_gcode.svg_parser import parse_file

router = APIRouter(
    prefix="/api/gcode",
    tags=["gcode"],
)


@router.get("/generate")
async def create_file(filename: str):
    print("filename,", filename)
    gcode_compiler = Compiler(interfaces.Gcode, movement_speed=1000, cutting_speed=300, pass_depth=5)

    curves = parse_file(f"./uploaded_files/{filename}", canvas_height=100)  # Parse an svg file into geometric curves

    gcode_compiler.append_curves(curves)
    compiled_gcode = gcode_compiler.compile()
    return {"gcode": compiled_gcode}