import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


def create_app():
    app = FastAPI()
    origins = [
        "*",
    ]

    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        expose_headers=["*"],
    )
    return app


app = create_app()


@app.on_event("startup")
def register_routes():
    from endpoints import files
    from endpoints import gcode
    app.include_router(files.router)
    app.include_router(gcode.router)



def run_dev_server():
    uvicorn.run(
        app="app:app",
        host="0.0.0.0",
        port=5000,
        reload=True,
        workers=4,
    )


if __name__ == "__main__":
    run_dev_server()
